FROM registry.access.redhat.com/ubi7/ubi-init
LABEL maintainer="Maarten"
ENV container=docker

# Install requirements.
RUN yum makecache fast \
    && yum -y update \
    && yum install -y \
        hostname \
        policycoreutils-python-utils \
        python3 \
        python3-libselinux \
        sudo \
        which \
    && sed -i 's/plugins=0/plugins=1/g' /etc/yum.conf \
    && yum clean all

# Create `ansible` user with sudo permissions and membership in `DEPLOY_GROUP`
ENV ANSIBLE_USER=ansible SUDO_GROUP=wheel DEPLOY_GROUP=deployer
RUN set -xe \
  && groupadd -r ${ANSIBLE_USER} \
  && groupadd -r ${DEPLOY_GROUP} \
  && useradd -m -g ${ANSIBLE_USER} ${ANSIBLE_USER} \
  && usermod -aG ${SUDO_GROUP} ${ANSIBLE_USER} \
  && usermod -aG ${DEPLOY_GROUP} ${ANSIBLE_USER} \
  && sed -i "/^%${SUDO_GROUP}/s/ALL\$/NOPASSWD:ALL/g" /etc/sudoers

VOLUME ["/sys/fs/cgroup"]
CMD ["/usr/sbin/init"]
